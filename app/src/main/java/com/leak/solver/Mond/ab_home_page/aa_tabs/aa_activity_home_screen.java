package com.leak.solver.Mond.ab_home_page.aa_tabs;

import android.Manifest;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.leak.solver.Mond.R;
import com.leak.solver.Mond.aa_StartUp.aa_activity_StartUp;

import com.leak.solver.Mond.ab_home_page.FileManager_Home;
import com.leak.solver.Mond.ab_home_page.aa_tabs.ab_favorites.FileManager_Home_fav;
import com.leak.solver.Mond.ab_home_page.aa_tabs.ac_folderview.FileManager_Home_folder;
import com.leak.solver.Mond.zz_Config;

/*adb push ZZ_3gp_JOJO  /storage/emulated/0
*/

public class aa_activity_home_screen extends AppCompatActivity {

    public static aa_activity_home_screen                                            mContext=null;


    public aa_activity_StartUp                                                _global_StartUP =null;

    public FileManager_Home_fav                                           fileManager_home_fav=null;
    public FileManager_Home_folder fileManager_home_folder=null;

    FileManager_Home                                                         fileManager_home=null;

    Boolean   need_reload_dir = false;

    public  Thread threadS;

    public static aa_activity_home_screen getContext() {
        return mContext;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.ac_activity_aa_home_screen);

        /////////////////////////Initialization /////////////////////

        /*
        _global_StartUP = aa_activity_StartUp.aaStartUp.Get_obj();
        fileManager_home = new FileManager_Home();
        fileManager_home_fav = new FileManager_Home_fav();

        fileManager_home_folder = new FileManager_Home_folder();

        Thread_Init();
        */
       // fileManager_home_fav.enable_tab(aa_activity_home_screen.this);

       // fileManager_home_fav.enable_tab(aa_activity_home_screen.this);

        //  handler = new Handler(Looper.myLooper());



        if(zz_Config.TEST)
        {
            Log.d("_####_ON _CREATE", "onCreate: aa_activity_home_screen -- A");
        }



        fileManager_home = new FileManager_Home();
        fileManager_home_fav = new FileManager_Home_fav();
        fileManager_home_folder = new FileManager_Home_folder();
        fileManager_home.window_toolbar_navbar_bg(aa_activity_home_screen.this);

        _global_StartUP = aa_activity_StartUp.aaStartUp.Get_obj();
        fileManager_home.reset_variables(aa_activity_home_screen.this);
        fileManager_home.initialize(aa_activity_home_screen.this);
        fileManager_home.getPermission(aa_activity_home_screen.this);




//

        if(zz_Config.TEST)
            Log.d("_###_ADAPTER_THREAD", " Hash Inside AdapterThread :   HomePage"+ aa_activity_StartUp.aaStartUp);


        // func_test();

/*
        directories = new ArrayList<>();

        for (int i = 0; i <  0; i++)
        {

              DirectoryFav directoryFav = new DirectoryFav( "lol"+i, i + " Videos");
            directories.add(directoryFav);
        }

        if(zz_Config.TEST)
        {
            Log.d("_###ADAPTER_THREAD", " Hash Inside AdapterThread :   HomePage"+ Arrays.toString(directories.toArray()));

        }
 */

       // enable_tab();







        if(zz_Config.TEST)
        {
            Log.d("_####_ON _CREATE", "onCreate: aa_activity_home_screen -- B");

        }

        // Runtime.getRuntime().gc();


    }
    @Override
    protected void onResume()
    {


        if(zz_Config.TEST)
        {
            Log.d("_####_ON_RESUME", "onResume: aa_activity_home_screen -- A");
        }


/*

        fileManager_home.initialize(aa_activity_home_screen.this);

        if (ActivityCompat.checkSelfPermission(aa_activity_home_screen.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

         //   aa_activity_home_screen.mContext.fileManager_home_fav.Init_Full_Read_Dir(aa_activity_home_screen.this);
            if(need_reload_dir)
            {
                   aa_activity_home_screen.mContext.fileManager_home_fav.Init_Full_Read_Dir(aa_activity_home_screen.this);

            }
            else
                need_reload_dir=false;


        }

*/
        super.onResume();

        /*
        if(zz_Config.TEST)
        {
            Log.d("_####_ON_RESUME", "onResume: aa_activity_home_screen -- B");
            Log.d("_#APP_STATE_H",zz_Config.getAppTaskState(aa_activity_StartUp.aaStartUp.activityManager));

        }
*/


    }

    @Override
    protected void onUserLeaveHint()
    {

        /*
        if(zz_Config.TEST){
            Log.d("_####_ON_LEAVE", "onPause  :  aa_activity_home_screen A");

        }





        if (ActivityCompat.checkSelfPermission(aa_activity_home_screen.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

           Thread_Kiler(null);
            need_reload_dir=true;




        }
        else
        {
            fileManager_home.reset_variables(aa_activity_home_screen.this);
        }


 */
           super.onUserLeaveHint();


    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults)
    {
        fileManager_home.initialize(aa_activity_home_screen.this);


        if(zz_Config.TEST){
            Log.d("_####_ON_LEAVEP", "onRequestPermissionsResult  :  aa_activity_home_screen A");

        }
        fileManager_home.onRequestPermissionsResult(aa_activity_home_screen.this,requestCode,  permissions, grantResults);





    }



    @Override
    public void onBackPressed()
    {

        if(zz_Config.TEST)
            Log.d("_####_BACKBUTTON", " Inside onBackPressed :   aa_activity_home_screen A");


        fileManager_home.Thread_Kiler( );

        while (getFragmentManager().getBackStackEntryCount() > 0)
        {
            getFragmentManager().popBackStack(); // pop fragment here
        }
        zz_Config.IS_BACK=true;
         aa_activity_home_screen.mContext=null;


        super.onBackPressed(); // after nothing is there default behavior of android works.


        if(zz_Config.TEST)
            Log.d("_####_BACKBUTTON", " Inside onBackPressed :   aa_activity_home_screen B");


    }









/*
    @Override
    public void onBackPressed()
    {
        if(zz_Config.TEST)
            Log.d("_####_BACKBUTTON", " Inside onBackPressed :   aa_activity_home_screen A");


        Thread_Kiler(null);

        while (getFragmentManager().getBackStackEntryCount() > 0)
        {
            getFragmentManager().popBackStack(); // pop fragment here
        }
        zz_Config.IS_BACK=true;
        aa_activity_home_screen.mContext=null;
        super.onBackPressed(); // after nothing is there default behavior of android works.


        if(zz_Config.TEST)
            Log.d("_####_BACKBUTTON", " Inside onBackPressed :   aa_activity_home_screen B");


    }



    @Override
    protected void onPause() {


        if(zz_Config.TEST)
            Log.d("_####_ON_PAUSE", "onPause  :  aa_activity_home_screen A");





        if(zz_Config.TEST)
            Log.d("_####_ON_PAUSE", "onPause  :  aa_activity_home_screen B");
        super.onPause();

    }

    @Override
    public void onDestroy()
    {

        if(zz_Config.TEST)
            Log.d("_####_DESTROYP", "Destroy : aa_activity_home_screen A");

        //  finishAffinity();



        if(zz_Config.TEST)
            Log.d("_####_DESTROYP", "Destroy: aa_activity_home_screen B");

        super.onDestroy();

    }


*/




}
