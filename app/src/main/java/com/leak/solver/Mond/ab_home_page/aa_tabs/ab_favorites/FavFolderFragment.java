package com.leak.solver.Mond.ab_home_page.aa_tabs.ab_favorites;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.leak.solver.Mond.R;
import com.leak.solver.Mond.ab_home_page.aa_tabs.aa_activity_home_screen;
import com.leak.solver.Mond.ab_home_page.aa_tabs.ab_favorites.aa_Recycler.RecyclerAdapterFav;
import com.leak.solver.Mond.zz_Config;

/**
 * A simple {@link Fragment} subclass.
 */
public class FavFolderFragment extends Fragment {
    public static final String ARG_PAGE = "ARG_PAGE";
    private int mPage;
    int refresh_i=0;

    aa_activity_home_screen  aa_activity_home_screen =null;
    public FavFolderFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPage = getArguments().getInt(ARG_PAGE);

         aa_activity_home_screen = ((aa_activity_home_screen) getActivity());


        if (zz_Config.TEST) {
            Log.d("_#_FAV_FRAG_ON_CREATE", "onCreate fav fragment  : aa_activity_home_screen__ " + mPage);

        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = null;



        if (zz_Config.TEST) {
            Log.d("_#_FAV_FRAG_ON_VIEW", " onCreateView fav fragment  : aa_activity_home_screen__ " + mPage);

        }


        rootView = inflater.inflate(R.layout.ad_b_fragment_home_fav, container, false);

        if (zz_Config.TEST) {
            Log.d("_#_FAV_FRAG_ON_VIEW", " onCreateView fav fragment  : aa_activity_home_screen__ " + mPage);

        }


        rootView = inflater.inflate(R.layout.ad_b_fragment_home_fav, container, false);

        //   ((aa_activity_home_screen)getActivity()).back.setVisibility(View.VISIBLE);


        aa_activity_home_screen.fileManager_home_fav.recyclerViewFav =  rootView.findViewById(R.id.contact_recycleView);
        aa_activity_home_screen.fileManager_home_fav.recyclerViewFav.setHasFixedSize(true);
        aa_activity_home_screen.fileManager_home_fav.recyclerAdapterFav = new RecyclerAdapterFav(aa_activity_home_screen.fileManager_home_fav.directories, getContext());

        if (zz_Config.TEST)
            Log.d("_#_STOP A", "onRefresh fav fragment  : aa_activity_home_screen ");



        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        aa_activity_home_screen.fileManager_home_fav.recyclerViewFav.setLayoutManager(linearLayoutManager);



        aa_activity_home_screen.fileManager_home_fav.recyclerViewFav.setAdapter(aa_activity_home_screen.fileManager_home_fav.recyclerAdapterFav);


        // SwipeRefreshLayout
        aa_activity_home_screen.fileManager_home_fav.mSwipeRefreshLayoutFav = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_container);
        aa_activity_home_screen.fileManager_home_fav.mSwipeRefreshLayoutFav.setColorSchemeResources(R.color.colorAccent,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

        //   StartThread_dir_update(false);


        aa_activity_home_screen.fileManager_home_fav.mSwipeRefreshLayoutFav.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Your recyclerview reload logic function will be here!!!

                if (zz_Config.TEST) {
                    Log.d("_#_#FAV_FRAG_REFRESH", "onRefresh fav fragment  : aa_activity_home_screen "+aa_activity_home_screen.fileManager_home_fav.No_Read_Dir);

                }


                //Init_Full_Read_Dir_square

                if(aa_activity_home_screen.fileManager_home_fav.Full_Read_Dir!=null)
                {
                    aa_activity_home_screen._global_StartUP.ab_fileManagerStart.THREAD_STOPPER=true;
                    while (aa_activity_home_screen.fileManager_home_fav.Full_Read_Dir.isAlive()) ;
                    aa_activity_home_screen._global_StartUP.ab_fileManagerStart.THREAD_STOPPER=false;
                }


                aa_activity_home_screen.mContext.fileManager_home_fav.Init_Full_Read_Dir(aa_activity_home_screen);


               // aa_activity_home_screen.fileManager_home_fav.Init_Full_Read_Dir_square(aa_activity_home_screen._global_StartUP);


/*

                if(aa_activity_home_screen.threadS!=null) {
                    aa_activity_home_screen.threadS.interrupt();
                    while (aa_activity_home_screen.threadS.isAlive()) ;
                }




                aa_activity_home_screen.func_test(refresh_i++);
*/


                // DirUpdateThreadFromFrag();

                // StartThread_dir_update(true);

                aa_activity_home_screen.fileManager_home_fav.mSwipeRefreshLayoutFav.setRefreshing(false);

            }
        });






        return rootView;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {


        super.onViewCreated(view, savedInstanceState);
    }

    public void refresh(View view) {


        /*
        if(zz_Config.TEST)
        {
            Log.d("_#_FAV_FRAG_REFRESH", "refresh fav fragment    : aa_activity_home_screen A__"+mPage);

        }

        StartThread_dir_update(true);

        if(zz_Config.TEST)
        {
            Log.d("_#_FAV_FRAG_REFRESH", "refresh fav fragment    : aa_activity_home_screen B__  "+mPage);

        }
*/

    }


    // public


}
