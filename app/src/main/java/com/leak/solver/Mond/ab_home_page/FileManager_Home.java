package com.leak.solver.Mond.ab_home_page;


import android.Manifest;
import android.app.ActivityManager;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;

import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;


import com.leak.solver.Mond.R;
import com.leak.solver.Mond.aa_StartUp.aa_activity_StartUp;
import com.leak.solver.Mond.ab_home_page.aa_tabs.aa_activity_home_screen;
import com.leak.solver.Mond.zz_Config;

public class FileManager_Home {

    public Toolbar                                                                     toolbar=null;
    final int                                           MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 20;
    public final ActivityManager                                                       activityManager=null;

    public void window_toolbar_navbar_bg(AppCompatActivity appCompatActivity)
    {
        Window window = appCompatActivity.getWindow();
        Drawable background = appCompatActivity.getResources().getDrawable(R.drawable.ab_toolbar_bg);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(appCompatActivity.getResources().getColor(android.R.color.transparent));
        window.setNavigationBarColor(appCompatActivity.getResources().getColor(android.R.color.transparent));
        window.setBackgroundDrawable(background);


        toolbar = (Toolbar) appCompatActivity.findViewById(R.id.toolbar);
        appCompatActivity.setSupportActionBar(toolbar);
        appCompatActivity.getSupportActionBar().setDisplayShowTitleEnabled(false);

    }


    public void getPermission(AppCompatActivity appCompatActivity)
    {

        if (ActivityCompat.checkSelfPermission(appCompatActivity, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
        {

            if(zz_Config.TEST)
            {
                Log.d("_#GOT_PERMISSION", "gotPermission --getPermission : aa_activity_home_screen -- A");
            }
             //setFirstTime_permission(appCompatActivity);
            //StartUpScreenThread_dir_fill(false);

            //  enable_tab();

            //    StartThread_dir_update(false);

            //StartThread_dir_update(false);

          //  aa_activity_home_screen.mContext.fileManager_home_fav.StartUpScreenThread_dir_fill(false,false,appCompatActivity);


            //aa_activity_home_screen.mContext.fileManager_home_fav.Init_Half_Read_Dir(appCompatActivity);

            if(zz_Config.TEST)
            {
                Log.d("_#GOT_PERMISSION", "gotPermission --getPermission : aa_activity_home_screen -- B");
            }



        }
        else
        {


            if (ActivityCompat.shouldShowRequestPermissionRationale(appCompatActivity, Manifest.permission.READ_EXTERNAL_STORAGE))
            {


                permission_snack_message(appCompatActivity);
                if(zz_Config.TEST)
                {
                    Log.d("_#NO_PERMISSION_PREV", "shouldShowRequestPermissionRationale --getPermission : aa_activity_home_scree");
                }

            }
            else
            {
                if(zz_Config.TEST)
                {
                    Log.d("_#NO_PERMISSION_FIRST", "noPermission --getPermission : aa_activity_home_screen -- A");
                }

                ActivityCompat.requestPermissions(appCompatActivity,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);

                if(zz_Config.TEST)
                {
                    Log.d("_#NO_PERMISSION_FIRST", "noPermission --getPermission : aa_activity_home_screen -- B");
                }


            }
        }


    }

    public void permission_snack_message(final AppCompatActivity appCompatActivity)
    {



        Snackbar.make(appCompatActivity.findViewById(android.R.id.content),
                "Needs permission to read  video",
                Snackbar.LENGTH_INDEFINITE).setAction("ENABLE",
                new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {

                        ActivityCompat.requestPermissions(appCompatActivity, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);

                        if(zz_Config.TEST)
                        {
                            Log.d("_#SNACK_BAR", "permission_snack_message : aa_activity_home_screen");
                        }

                    }
                }).show();
    }

    private void setFirstTime_permission(AppCompatActivity appCompatActivity)
    {


        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(appCompatActivity);

        // first time
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("Permission", true);
        editor.commit();
        boolean ranBefore = preferences.getBoolean("Permission", false);

        if(zz_Config.TEST)
            Log.d("_#FIRST_FLAG", "isFirstTime shared pref : aa_activity_home_screen " + ranBefore);




    }



    public void onRequestPermissionsResult(AppCompatActivity appCompatActivity,int requestCode, String permissions[], int[] grantResults)
    {

        switch (requestCode)
        {


            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
            {

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    //                    grantResult[0] means it will check for the first postion permission which is READ_EXTERNAL_STORAGE
                    //                    grantResult[1] means it will check for the Second postion permission which is CAMERA
                    Toast.makeText(appCompatActivity, "Permission Granted", Toast.LENGTH_SHORT).show();

                    if(zz_Config.TEST){
                        Log.d("_#PERMISSION_RESULT", "granted onRequestPermissionsResult : aa_activity_home_screen -- A");
                    }

                    setFirstTime_permission(appCompatActivity);
                   // aa_activity_home_screen.mContext.fileManager_home_fav.StartUpScreenThread_dir_fill(true,false,appCompatActivity);
                    //aa_activity_home_screen.mContext.fileManager_home_fav.Init_Full_Read_Dir(appCompatActivity);
                    //aa_activity_home_screen.mContext.fileManager_home_fav.Full_Read_Dir.start();
                   // aa_activity_home_screen.mContext.fileManager_home_fav.Init_Full_Read_Dir(appCompatActivity);
                    if(zz_Config.TEST){
                        Log.d("_#PERMISSION_RESULT", "granted onRequestPermissionsResult : aa_activity_home_screen -- B");
                    }


                }
                else
                {


                    permission_snack_message(appCompatActivity);

                }

            }
        }


    }


    public void initialize(AppCompatActivity appCompatActivity)
    {


        aa_activity_home_screen.mContext=(aa_activity_home_screen)appCompatActivity;
        aa_activity_StartUp.aaStartUp = aa_activity_home_screen.mContext._global_StartUP;




    }


    public void reset_variables(AppCompatActivity appCompatActivity)
    {

        aa_activity_StartUp.aaStartUp=null;
        aa_activity_home_screen.mContext=null;
        Runtime.getRuntime().gc();

    }

    public void Thread_Kiler()
    {


        Log.d("_#_TT_TEST", " first  Alive test"+aa_activity_home_screen.mContext.fileManager_home_fav.Full_Read_Dir);

        int j=0;
        if(aa_activity_home_screen.mContext.fileManager_home_fav.Full_Read_Dir!=null)
        {

            if(aa_activity_home_screen.mContext.fileManager_home_fav.Full_Read_Dir.isAlive())
            {
                aa_activity_home_screen.mContext._global_StartUP.ab_fileManagerStart.THREAD_STOPPER =true;
            }

            while (aa_activity_home_screen.mContext.fileManager_home_fav.Full_Read_Dir.isAlive())
            {

                if(zz_Config.TEST)
                    Log.d("_#_TT_TEST", " first  Alive test"+aa_activity_home_screen.mContext.fileManager_home_fav.Full_Read_Dir.isAlive()+":"+j++);
            }


        }


        j=0;
        if(aa_activity_home_screen.mContext.fileManager_home_fav.No_Read_Dir!=null)
        {

            if(aa_activity_home_screen.mContext.fileManager_home_fav.No_Read_Dir.isAlive())
            {
                aa_activity_home_screen.mContext._global_StartUP.ab_fileManagerStart.THREAD_STOPPER =true;

            }

            while (aa_activity_home_screen.mContext.fileManager_home_fav.No_Read_Dir.isAlive())
            {

                  if(zz_Config.TEST)
                    Log.d("_#_TT_TEST", " first  Alive test"+aa_activity_home_screen.mContext.fileManager_home_fav.No_Read_Dir.isAlive()+"::"+j++);
            }


        }









        //fileManager_home_fav.Init_Full_Read_Dir_Reset();
        //  fileManager_home_fav.Init_Half_Read_Dir_Reset();
        aa_activity_home_screen.mContext._global_StartUP.ab_fileManagerStart.THREAD_STOPPER =false;

        //    fileManager_home.reset_variables(aa_activity_home_screen.this);


    }




}
